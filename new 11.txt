1.Compile time polymorphysm :

Function Overloading : same name diff params ==> void func(int x) , void func(double x)

Operator Overloading : 
	// Operator Overloading
		classtype operator+(clstype const& obj)
		{
			
			return classtype((memb+obj.memb),(memb1+obj.memb1));
		}
	};
 
    classtype c1(10, 5), c2(2, 4);
 
    // An example call to "operator+"
    classtype c3 = c1 + c2;
 
 
 2. Runtime Polymorphism : The function call is resolved at runtime in runtime polymorphism
 
A. Function Overriding : 

	class parent {
	 public: func(){statements;}
	 public: data;
	};
	class child: parent{
	public : func(){statements;}
	public : data;
	};
 
Note : Runtime Polymorphism cannot be achieved by data members in C++. ==> eg in above if :
		parent x = child(); 
		x.func()==> here func of the child is called..
		x.data ; ==> however here the data of the parent in accessed,,

B. Virtual Function : here the member function declared in parent is declared with virtual keyword, means the child will override it..
					  and now the parent object also possess the vitual function child class..( runtime bind) how ever non virtual is binded at comiple time..
					  
	 eg : // C++ program for function overriding with data members
		class Parent { 
		public:
			 void say() {
				std::cout << "1\n";
			}
			 virtual void fun() {
				std::cout << "1\n";
			}
		};

		class Child : public Parent {
		public:
			void say()
			{
				std::cout << "2\n";
			}
		  void fun() {
			  std::cout << "2\n";
		  }
		};

		int main()
		{
		   Parent* a = new Child();
			a->say();  //print 1 ( parents) //compile time bind..
			a->fun();  // print 2 ( child)  // runtime bind..
		  return 0;
		}
		
* Virtual distructor.. :
class base {
  public:
    base()     
    { cout << "Constructing base\n"; }
    virtual ~base()
    { cout << "Destructing base\n"; }     
};
 
class derived : public base {
  public:
    derived()     
    { cout << "Constructing derived\n"; }
    ~derived()
    { cout << "Destructing derived\n"; }
};
 * to de allocate both base and derived..
 Deleting a derived class object using a pointer of base class type that has a non-virtual destructor results in undefined behavior.
 To correct this situation, the base class should be defined with a virtual destructor. 
 
 
* Pure virtual function and abstract class:  A pure virtual function is declared by assigning 0 in the declaration. ==>
											A class is abstract if it has at least one pure virtual function.
											// Error: Cannot instantiate an abstract class
											
* Inheritance :  
				1. class ABC : private XYZ              //private derivation
							{                }
				2. class ABC : public XYZ              //public derivation
							{               }
				3. class ABC : protected XYZ              //protected derivation
							{              }
				4. class ABC: XYZ                            //private derivation by default
				{            }

				Note:
				*  When a base class is privately inherited by the derived class, 
				public members of the base class becomes the private members of the derived class and therefore, 
				the public members of the base class can only be accessed by the member functions of the derived class.
				They are inaccessible to the objects of the derived class.
				*  On the other hand, when the base class is publicly inherited by the derived class,
				public members of the base class also become the public members of the derived class.
				Therefore, the public members of the base class are accessible by the objects of the derived class as well
				as by the member functions of the derived class.
				
				*   Public Mode: If we derive a subclass from a public base class. Then the public member of the base class will
				become public in the derived class and protected members of the base class will become protected in the derived class.
				
				*   Protected Mode: If we derive a subclass from a Protected base class. Then both public members and protected 
				members of the base class will become protected in the derived class.
				
				*   Private Mode: If we derive a subclass from a Private base class. Then both public members and protected members
				of the base class will become Private in the derived class.
 

			encapsulation
			abstraction
			polymorphism
			inheritance
			
			
			
			
			
			
			
			
			
			
			
			
			Introduction to std::vector

	std::vector<type> name 
	std::vector<int> data( 10 ); // vector containing 10 int elements, value-initialized to 0
	std::vector<int> data { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; // vector containing 10 int values
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	ptr = malloc(*int)(size)
ptr = calloc(*int(num,size)
ptr = realloc(*int)(src_ptr,newsize)
free(ptr)	

pointer-variable = new data-type;
ptr = new int; //ptr = new int(12);
delete pointer-variable;
delete p;

pointer-variable = new data-type[size];
int *p = new int[10]
delete[] pointer-variable;  
delete[] p;

“new” does call the constructor of a class whereas “malloc()” does not.

free() frees memory but doesn’t call Destructor of a class whereas “delete” frees the memory and also calls the Destructor of the class.

shallow copy : all the elements of the objects are copied to the new obj..
 Eg : defualt copy constructor. it does the shallow copy.. 
  obj* a = new obj();
  obj b = a ; // default copy constructor called .. here shallow copy.. 
  so now both b and a refer to same obj.. so any change in a changes in b..
  Since both objects will reference to the same memory location, then change made by one will reflect those change in another object as well.
  defualt copy constructor --> shallow copy..
	
Deep Copy :
	check : https://www.linkedin.com/pulse/understanding-deep-copy-shallow-c-object-oriented-yamil-garcia-qaoze/ nice..
	  		class cls
		{
			private : 
			int *d1;
			
	cls(const cls& clsvar) //deep copy..
	: d1{new int(*(clsvar.d1)) }  //deep copy..
	{}  
opertor overloading : 	
		classname op+(classname const& obj)
		{
		return data + obj.data;
		}
		if use the frnd function :

		classname op+(classname obj1, classname obj2)
		{
		return obj1.data + obj2.data;
		}
		
		
STL :::::::::::


dynamic array using vectors:

		std::vector<type>name;
		vector<dataType> name({ value1, value2, value3 ....}); 
		vector<dataType> name(size, value); 
		vector<dataType> name(other_vec);


		vector<int>::iterator ptr; 
		
Linked list using list and forward list..

	list : double linked list..
	forward list : single linked list..
	
	std::list <data-type> name_of_list;
	
			Points to Remember about List Container 
			It is generally implemented using a dynamic doubly linked list with traversal in both directions.
			Faster insert and delete operation as compared to arrays and vectors.
			It provides only sequential access. Random Access to any middle element is not possible
			It is defined as a template so it is able to hold any data type.
			It operates as an unsorted list would, which implies that by default, the list’s order is not preserved. However, there are techniques for sorting.
			
			To insert a new node at the beginning of the doubly list, we can use the following steps:

			Allocate memory for a new node (say new_node) and assign the provided value to its data field.
			Set the previous pointer of the new_node to nullptr.
			If the list is empty:
			Set the next pointer of the new_node to nullptr.
			Update the head pointer to point to the new_node.
			If the list is not empty:
			Set the next pointer of the new_node to the current head.
			Update the previous pointer of the current head to point to the new_node.
			Update the head pointer to point to the new_node.
	forward_list<type> name;
	
		// Assigning values using assign()
		flist1.assign({ 1, 2, 3 });
		// Assigning repeating values using assign()
		// 5 elements with value 10
		flist2.assign(5, 10);
		//Assigning values of list 1 to list 3
		flist3.assign(flist1.begin(), flist1.end());\
		 // Initializing forward list
		forward_list<int> flist = { 10, 20, 30, 40, 50 };
		
		// Declaring a forward list iterator
		forward_list<int>::iterator ptr;
		
Deque in C++ Standard Template Library (STL):
	
	deque<int> gquiz;
	
	deque<int>::iterator it;
	
	A block of memory is automatically allocated when a deque object is created so that the objects can be stored in contiguous locations.

	Deque then allocates a new block of memory and joins the front of the prior memory block with it when we put an item in front of it.
	Now, if we add pieces to the front once more, they will be stored in this new memory block until it is entirely full.
	
	When an item is inserted at the end of a deque, the allocated block of memory holds it until it is completely filled;
	if this occurs, a new block of memory is allocated and connected to the end of the preceding block.
	Elements that are added to the deque’s back are now stored in that new memory block.
	
	Accessing Elements- O(1)
	Insertion or removal of elements- O(N)
	Insertion or removal of elements at start or end- O(1)
	

Array class in C++ :
	
	array<int,6> ar;//fixed size...
	Array classes knows its own size, whereas C-style arrays lack this property. So when passing to functions, we don’t need to pass size of Array as a separate parameter.
	With C-style array there is more risk of array being decayed into a pointer. Array classes don’t decay into pointers
	Array classes are generally more efficient, light-weight and reliable than C-style arrays.
	
	
	 map<key type, val type> mp;
	 
	 
Multi thread :
	
	void foo(param)
	{ 
	  Statements; 
	}
	// The parameters to the function are put after the comma
	std::thread thread_obj(foo, params);
	
POSIX thread:

	pthread_create (thread, attr, start_routine, arg);
	
	1	thread : An opaque, unique identifier for the new thread returned by the subroutine.

	2	attr: An opaque attribute object that may be used to set thread attributes. You can specify a thread attributes object, or NULL for the default values.

	3	start_routine :The C++ routine that the thread will execute once it is created.

	4	arg :A single argument that may be passed to start_routine. It must be passed by reference as a pointer cast of type void. NULL may be used if no argument is to be passed.
	
			#include <chrono>
			#include <iostream>
			#include <thread>
			void first() {
			  std::cout << "111111111111." << std::endl;
			  std::this_thread::sleep_for(std::chrono::seconds(10));
			  std::cout << "333333333." << std::endl;
			}
			void second() {
			  std::cout << "22222222222222." << std::endl;
			  std::this_thread::sleep_for(std::chrono::seconds(4));
			  std::cout << "444444444444." << std::endl;
			}
			int main() {
			  std::cout << "Welcome To My Domain Starting the first thread.\n";
			  std::thread example(first);
			  std::cout << "Welcome To My Domain Starting the second thread...\n";
			  std::thread example1(second);
			  std::cout << "Thanks users we will waiting for the threads completion..."
						<< std::endl;
			  example.join(); //thread1 starts here
			  example1.join(); // thread 2 starts here.
			  std::cout << "Thread completion is over !\n"; //here since we have used the join before coming here both threads are executed.. exit of 1 thread doesn't cause to come here..
			}
			
			//output : ~/cpppractice$ ./a.out 
						Welcome To My Domain Starting the first thread.
						Welcome To My Domain Starting the second thread...
						111111111111.
						22222222222222.
						Thanks users we will waiting for the threads completion...
						444444444444.
						333333333.
						Thread completion is over !
						~/cpppractice$ ^C
						~/cpppractice$ 
			
			
			
			
			
			
			
			
			
			
			
			classname op+(classname const& obj)
{
return data + obj.data;
}
if use the frnd function :

classname op+(classname obj1, classname obj2)
{
return obj1.data + obj2.data;
}


std::vector<type>name;
vector<dataType> name({ value1, value2, value3 ....}); 
vector<dataType> name(size, value); 
vector<dataType> name(other_vec);


vector<int>::iterator ptr; 












_exit()
_exit
•will terminate a process, specifically the API will cause the calling process data segment and U Area to be deallocated and all the open file descriptor to be closed.
	However the process table slot entry for this process is still intact so that the process exit status and its
	
•execution statistics are recorded in Process table entry this needs to be cleared by the parent process, if its not cleared the process will become zombie.